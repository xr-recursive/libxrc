#include "color.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int hexdigit(char c) {
	assert(c >= '0');
	if (c <= '9') {
		return c - '0';
	}
	c |= 0x20; // to lowercase
	assert(c >= 'a');
	assert(c <= 'f');
	return c - 'a' + 10;
}

/**
 * Parse a color
 *
 * There are several formats colors can be in
 * 
 * Hex color codes are of the form #rrggbb or #rrggbbaa
 *
 * Float color codes are of the form $r.rrrrr,g.ggggg,b.bbbbb,a.aaaaa
 *
 * Named color codes are found in /etc/xr/rgb.txt
 * /etc/xr/rgb.txt is designed to be linked to /etc/X11/rgb.txt
 */
XrColor xrc_parse(const char *s) {
	XrColor out;
	out.a = 1.0;
	FILE *f;
	switch (s[0]) {
	case '#':
		switch (strlen(s)) {
		case 9:
			out.a = (float) ((hexdigit(s[7]) << 4) + hexdigit(s[8]))
				/ 255.0;
		case 7:
			out.r = (float) ((hexdigit(s[1]) << 4) + hexdigit(s[2]))
				/ 255.0;
			out.g = (float) ((hexdigit(s[3]) << 4) + hexdigit(s[4]))
				/ 255.0;
			out.b = (float) ((hexdigit(s[5]) << 4) + hexdigit(s[6]))
				/ 255.0;
		}
		break;
	case '$':
		sscanf(s, "$%f,%f,%f,%f", &out.r, &out.g, &out.b, &out.a);
		break;
	default:
		f = fopen("/etc/xr/rgb.txt", "r");
		while (1) {
			int r, g, b;
			char n[32];
			if (fscanf(f, "%u %u %u %31[^\n]\n", &r, &g, &b, n) != 4) {
				continue;
			}
			out.r = (float) r / 255.0;
			out.g = (float) g / 255.0;
			out.b = (float) b / 255.0;
			break;
		}
	}
	return out;
}

char *xrc_serial(XrColor c) {
	char *s = malloc(128);
	sprintf(s, "$%f,%f,%f,%f", c.r, c.g, c.b, c.a);
	return realloc(s, strlen(s) + 1);
}

XrColor xrc_rgba(uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
	XrColor out;
	out.r = r;
	out.g = g;
	out.b = b;
	out.a = a;
	return out;
}

XrColor xrc_rgb(uint8_t r, uint8_t g, uint8_t b) {
	return xrc_rgba(r, g, b, 255);
}

static XrColor rgba_f(float r, float g, float b, float a) {
	XrColor out;
	out.r = r * 255.0;
	out.g = g * 255.0;
	out.b = b * 255.0;
	out.a = a * 255.0;
	return out;
}

XrColor xrc_hsla(float h, float s, float l, float a) {
	float c = (1. - abs(2.0 * l - 1.)) * s;
	float hp = fabs(h) / M_PI * 3.0;
	float x = c * (1. - abs(fmod(hp, 2.) - 1.));
	float r, g, b;
	r = 0.0; g = 0.0; b = 0.0;
	switch ((int) ceill(hp) % 6) {
	case 1:
		r = c;
		g = x;
		break;
	case 2:
		r = x;
		g = c;
		break;
	case 3:
		g = c;
		b = x;
		break;
	case 4:
		g = x;
		b = c;
		break;
	case 5:
		r = x;
		b = c;
		break;
	case 0:
		r = c;
		b = x;
		break;
	}
	float m = l - c / 2;
	return rgba_f(r + m, g + m, b + m, a);
}

XrColor xrc_hsl(float h, float s, float l) {
	return xrc_hsla(h, s, l, 1.0);
}

XrColor xrc_hsva(float h, float s, float v, float a) {
	float c = v * s;
	float hp = fabs(h) / M_PI * 3.0;
	float x = c * (1. - abs(fmod(hp, 2.) - 1.));
	float r, g, b;
	r = 0.0; g = 0.0; b = 0.0;
	switch ((int) ceill(hp) % 6) {
	case 1:
		r = c;
		g = x;
		break;
	case 2:
		r = x;
		g = c;
		break;
	case 3:
		g = c;
		b = x;
		break;
	case 4:
		g = x;
		b = c;
		break;
	case 5:
		r = x;
		b = c;
		break;
	case 0:
		r = c;
		b = x;
		break;
	}
	float m = v - c;
	return rgba_f(r + m, g + m, b + m, a);
}

XrColor xrc_hsv(float h, float s, float v) {
	return xrc_hsva(h, s, v, 1.0);
}
