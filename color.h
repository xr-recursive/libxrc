#pragma once

#include <stdint.h>

typedef struct {
	float r, g, b, a;
} XrColor;

XrColor xrc_parse(const char *str);
XrColor xrc_rgb(uint8_t r, uint8_t g, uint8_t b);
XrColor xrc_rgba(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
XrColor xrc_hsv(float h, float s, float v);
XrColor xrc_hsva(float h, float s, float v, float a);
XrColor xrc_hsl(float h, float s, float l);
XrColor xrc_hsla(float h, float s, float l, float a);

/*
XrColor xrc_hueshift(XrColor c, float h);
XrColor xrc_lightshift(XrColor c, float l);
XrColor xrc_satshift(XrColor c, float s);
*/

char *xrc_serial(XrColor c);
